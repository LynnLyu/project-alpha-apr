from django.forms import ModelForm
from projects.models import Project


# Create your models here.
class CreateForm(ModelForm):
    class Meta:
        model = Project
        fields = "__all__"
